package com.robco;

import java.awt.Point;
import java.awt.GridLayout;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.util.ArrayDeque;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class Terminal extends JFrame {
    private final String TITLE = "ROBCO TERMINAL SIMULATOR";
    private final int HEIGHT = 22;
    private final int WIDTH = 53;
    private final int W_BUFF = 4;
    private final int H_BUFF = 3;
    private final char WIDTH_LETTER = 'W';

    private Point pos = new Point(0, 0);
    private Color back = Color.white;
    private Color fore = Color.black;
    private JLabel[][] labels;
    private ArrayDeque<Integer> charBuff = new ArrayDeque<>();
    private boolean readChars = false;
    private Thread mainThread;

    public Terminal() {
        init();
    }

    public Terminal(Color background, Color foreground) {
        this.back = background;
        this.fore = foreground;
        init();
    }

    private void init() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(HEIGHT, WIDTH));
        panel.setBackground(this.back);

        this.labels = new JLabel[HEIGHT][WIDTH];
        for (int x = 0; x < HEIGHT; x++) {
            for (int y = 0; y < WIDTH; y++) {
                this.labels[x][y] = new JLabel();
                this.labels[x][y].setOpaque(true);
                this.labels[x][y].setBackground(this.back);
                this.labels[x][y].setForeground(this.fore);
                this.labels[x][y].setHorizontalAlignment(SwingConstants.CENTER);
                panel.add(this.labels[x][y]);
            }
        }

        this.add(panel);
        this.setTitle(TITLE);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);

        FontMetrics metrics = this.getGraphics().getFontMetrics();
        int cHeight = metrics.getHeight();
        int cWidth = metrics.charWidth(WIDTH_LETTER);
        int wHeight = (cHeight + H_BUFF) * HEIGHT;
        int wWidth = (cWidth + W_BUFF) * WIDTH;
        this.setResizable(false);
        this.setSize(wWidth, wHeight);

        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(new KeyDispatcher());
        this.mainThread = Thread.currentThread();

        this.clear();
    }

    private void advCurs() {
        if (this.pos.y + 1 >= WIDTH) {
            this.pos.y = 0;
            if (this.pos.x + 1 >= HEIGHT) {
                this.pos.x = 0;
            } else {
                this.pos.x++;
            }
        } else {
            this.pos.y++;
        }
    }

    private void sleep(long timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException ex) {}
    }

    public Point getCursorPos() {
        return new Point(this.pos.x, this.pos.y);
    }

    public Point setCursorPos(int x, int y) {
        if (x >= 0 && x < HEIGHT && y >= 0 && y < WIDTH) {
            this.pos = new Point(x, y);
        }
        return this.getCursorPos();
    }

    public Point setCursorPos(Point p) {
        return this.setCursorPos(p.x, p.y);
    }

    public Point write(char c) {
        return this.write(c, this.pos.x, this.pos.y, this.back, this.fore, true);
    }

    public Point write(char c, boolean moveCursor) {
        return this.write(c, this.pos.x, this.pos.y, this.back, this.fore, moveCursor);
    }

    public Point write(char c, Color back, Color fore) {
        return this.write(c, this.pos.x, this.pos.y, back, fore, true);
    }

    public Point write(char c, Color back, Color fore, boolean moveCursor) {
        return this.write(c, this.pos.x, this.pos.y, back, fore, moveCursor);
    }

    public Point write(char c, int x, int y, Color back, Color fore, boolean moveCursor) {
        if (moveCursor) {
            this.setCursorPos(x, y);
        }
        this.labels[x][y].setBackground(back);
        this.labels[x][y].setForeground(fore);
        this.labels[x][y].setText(Character.toString(c));
        if (moveCursor) {
            this.advCurs();
        }
        return this.getCursorPos();
    }

    public Point write(String s) {
        return this.write(s, this.pos.x, this.pos.y, this.back, this.fore, true);
    }

    public Point write(String s, boolean moveCursor) {
        return this.write(s, this.pos.x, this.pos.y, this.back, this.fore, moveCursor);
    }

    public Point write(String s, int x, int y) {
        return this.write(s, x, y, this.back, this.fore, true);
    }

    public Point write(String s, Color back, Color fore) {
        return this.write(s, this.pos.x, this.pos.y, back, fore, true);
    }

    public Point write(String s, int x, int y, boolean moveCursor) {
        return this.write(s, x, y, this.back, this.fore, moveCursor);
    }

    public Point write(String s, int x, int y, Color back, Color fore, boolean moveCursor) {
        Point orig = this.pos;
        this.setCursorPos(x, y);
        for (char c : s.toCharArray()) {
            this.write(c, this.pos.x, this.pos.y, back, fore, true);
        }
        if (!moveCursor) {
            this.setCursorPos(orig);
        }
        return this.getCursorPos();
    }

    public char read() {
        return this.read(this.pos.x, this.pos.y);
    }

    public char read(int x, int y) {
        return this.labels[x][y].getText().toCharArray()[0];
    }

    public char read(Point p) {
        return this.read(p.x, p.y);
    }

    public void clear() {
        this.setCursorPos(0, 0);
        for (int x = 0; x < HEIGHT; x++) {
            for (int y = 0; y < WIDTH; y++) {
                this.labels[x][y].setOpaque(true);
                this.labels[x][y].setBackground(this.back);
                this.labels[x][y].setForeground(this.fore);
                this.labels[x][y].setText(" ");
            }
        }
    }

    public int readKey() {
        this.readChars = true;
        while (this.charBuff.isEmpty()) {
            this.sleep(Long.MAX_VALUE);
        }
        this.readChars = false;
        int r = this.charBuff.pop();
        this.charBuff.clear();
        return r;
    }

    private class KeyDispatcher implements KeyEventDispatcher {
        @Override
        public boolean dispatchKeyEvent(KeyEvent e) {
            if (Terminal.this.readChars) {
                if (e.getID() == KeyEvent.KEY_RELEASED) {
                    Terminal.this.charBuff.push(e.getKeyCode());
                    Terminal.this.mainThread.interrupt();
                    return true;
                }
            }
            return false;
        }
    }
}
