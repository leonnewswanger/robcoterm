package com.robco;

public class Strings {
    public static final String ATTEMPT = "%d ATTEMPT(S) LEFT:";
    public static final String ROBCO = "ROBCO INDUSTRIES (TM) TERMLINK PROTOCOL";
    public static final String PASS = "ENTER PASSWORD NOW";
    public static final String DENIED = "Entry denied";
    public static final String GRADE = "%d/%d correct.";
    public static final String LOCKED = "TERMINAL LOCKED";
    public static final String ADMIN = "PLEASE CONTACT AN ADMINISTRATOR";
    public static final String BOX = "\u2588";
    public static final String[] CORRECT = {
        "Exact match!",
        "Please wait",
        "while system",
        "is accessed."
    };

    public static final String[] WORDS = {
        "FALLOUT", "SELLING", "CURRENT", "BARRENS", "SERVANT",
        "BARRAGE", "FALLING", "RANKING", "WALKING", "CARAVAN"
    };

    public static String attemptBuilder(int attempts) {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format(ATTEMPT, attempts));
        int i = 0;
        for (; i < attempts; i++) {
            builder.append(" " + BOX);
        }
        for (; i < 4; i++) {
            builder.append("  ");
        }
        return builder.toString();
    }
}
