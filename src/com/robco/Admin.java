package com.robco;

import com.robco.util.*;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.KeyEvent;

public class Admin {
    private static final Color FORE = Color.green;
    private static final Color BACK = new Color(0, 20, 0);
    private static final int ADDR_OFFSET_LEFT = 0;
    private static final int ADDR_OFFSET_RIGHT = 20;
    private static final int LEFT_COL_MIN = 7;
    private static final int LEFT_COL_MAX = 18;
    private static final int RIGHT_COL_MIN = 27;
    private static final int RIGHT_COL_MAX = 38;
    private static final int RAND_LEN_LOW = 20;
    private static final int RAND_LEN_HIGH = 28;
    private static final int COL_TOP = 5;
    private static final int COL_WIDTH = 12;
    private static final int ROWS = 17;
    private static final int TERM_BOTTOM = 21;
    private static final int OUT_COL_MAX = 52;
    private static final int OUT_COL_MIN = 40;
    private static final int OUT_ROWS = ROWS - 2;
    private static final int ADDR_LOWER = 0xF000;
    private static final int ADDR_UPPER = 0xFE00;

    private static Terminal term = new Terminal(BACK, FORE);
    private static Bounds leftBounds = new Bounds(LEFT_COL_MIN, LEFT_COL_MAX,
      COL_TOP, TERM_BOTTOM);
    private static Bounds rightBounds = new Bounds(RIGHT_COL_MIN, RIGHT_COL_MAX,
      COL_TOP, TERM_BOTTOM);
    private static Bounds outputBounds = new Bounds(OUT_COL_MIN, OUT_COL_MAX,
      COL_TOP, TERM_BOTTOM);
    private static int attempts = 4;
    private static TextRange selected;

    private static Bounds getCurrentBounds() {
        Point pos = term.getCursorPos();
        return pos.y >= rightBounds.L() ? rightBounds : leftBounds;
    }

    private static void drawAddrs() {
        int addr = Utility.randomInt(ADDR_LOWER, ADDR_UPPER);
        for (int i = 0; i < ROWS; i++) {
            int row = i + COL_TOP;

            int leftAddr = i * COL_WIDTH + addr;
            term.write(Utility.toHexString(leftAddr), row, ADDR_OFFSET_LEFT);

            int rightAddr = (i + ROWS) * COL_WIDTH + addr;
            term.write(Utility.toHexString(rightAddr), row, ADDR_OFFSET_RIGHT);
        }
    }

    private static void writeInColumn(String s, Bounds col) {
        writeInColumn(s, term.getCursorPos(), BACK, FORE, true, col);
    }

    private static void writeInColumn(String s, Point p, Bounds col) {
        writeInColumn(s, p, BACK, FORE, true, col);
    }

    private static void writeInColumn(String s, Point p, boolean moveCursor, Bounds col) {
        writeInColumn(s, p, BACK, FORE, moveCursor, col);
    }

    private static void writeInColumn(String s, Point p, Color back, Color fore, Bounds col) {
        writeInColumn(s, p, back, fore, true, col);
    }

    private static void writeInColumn(String s, Point p, Color back, Color fore,
      boolean moveCursor, Bounds col) {
        Point orig = term.getCursorPos();
        term.setCursorPos(p);
        for (char c : s.toCharArray()) {
            Point pos = term.write(c, back, fore, false);
            Point nPos = col.getBoundedResult(pos, Bounds.Direction.RIGHT);
            if (nPos.equals(pos)) {
                break;
            } else {
                term.setCursorPos(nPos);
            }
        }
        if (!moveCursor) {
            term.setCursorPos(orig);
        }
    }

    private static String readInColumn(Point first, Point last, Bounds col) {
        StringBuilder builder = new StringBuilder();
        Point pos = new Point(first.x, first.y);
        Point nPos = new Point(first.x, first.y);
        do {
            pos = new Point(nPos.x, nPos.y);
            builder.append(term.read(pos));
            nPos = col.getBoundedResult(pos, Bounds.Direction.RIGHT);
            if (nPos.equals(pos)) {
                break;
            }
        } while (!pos.equals(last));
        return builder.toString();
    }

    private static Point getCharPos(Point start, Bounds.Direction d) {
        Bounds col = getCurrentBounds();
        Point pos = new Point(start.x, start.y);
        Point last = new Point(start.x, start.y);
        char c = term.read(pos);
        while (Utility.isAlpha(c)) {
            last = new Point(pos.x, pos.y);
            Point nPos = col.getBoundedResult(pos, d);
            if (nPos.equals(pos)) {
                break;
            } else {
                pos = nPos;
                c = term.read(pos);
            }
        }
        return last;
    }

    private static void selectText() {
        Point pos = term.getCursorPos();
        Point first = getCharPos(pos, Bounds.Direction.LEFT);
        Point last = getCharPos(pos, Bounds.Direction.RIGHT);
        Bounds b = getCurrentBounds();
        String text = readInColumn(first, last, b);
        writeInColumn(text, first, FORE, BACK, b);
        writeOutputLine(0, Utility.padString(">" + text, outputBounds.R() - outputBounds.L()));
        selected = new TextRange(first, last, text);
    }

    private static void unselectText() {
        writeInColumn(selected.text(), selected.left(), BACK, FORE, getCurrentBounds());
    }

    private static void drawColumn(String[] words, int iStart, int iEnd, Bounds b) {
        term.setCursorPos(b.T(), b.L());
        for (int i = iStart; i < iEnd; i++) {
            int buffLen = Utility.randomInt(RAND_LEN_LOW, RAND_LEN_HIGH);
            String buff = Utility.randomString(buffLen);
            writeInColumn(buff + words[i], b);
        }
        writeInColumn(Utility.randomString(75), b);
    }

    private static void drawColumns(String[] words) {
        int half = words.length / 2;
        drawColumn(words, 0, half, leftBounds);
        drawColumn(words, half, words.length, rightBounds);
    }

    private static String getGuess() {
        loop: while (true) {
            Point nPos = term.getCursorPos();
            Bounds col = getCurrentBounds();
            switch (term.readKey()) {
                case KeyEvent.VK_ENTER:
                    if (selected.text().length() > 1)
                        return selected.text();
                    else
                        continue loop;
                case KeyEvent.VK_LEFT:
                    if (col.canMove(selected.left(), Bounds.Direction.LEFT))
                        nPos = col.getBoundedResult(selected.left(), Bounds.Direction.LEFT);
                    else if (selected.left().y == rightBounds.L())
                        nPos = new Point(selected.left().x, leftBounds.R());
                    else
                        continue loop;
                    break;
                case KeyEvent.VK_RIGHT:
                    if (col.canMove(selected.right(), Bounds.Direction.RIGHT))
                        nPos = col.getBoundedResult(selected.right(), Bounds.Direction.RIGHT);
                    else if (selected.right().y == leftBounds.R())
                        nPos = new Point(selected.right().x, rightBounds.L());
                    else
                        continue loop;
                    break;
                case KeyEvent.VK_UP:
                    if (col.canMove(selected.left(), Bounds.Direction.UP))
                        nPos = col.getBoundedResult(selected.left(), Bounds.Direction.UP);
                    else
                        continue loop;
                    break;
                case KeyEvent.VK_DOWN:
                    if (col.canMove(selected.right(), Bounds.Direction.DOWN))
                        nPos = col.getBoundedResult(selected.right(), Bounds.Direction.DOWN);
                    else
                        continue loop;
                    break;
                default:
                    continue loop;
            }
            unselectText();
            term.setCursorPos(nPos);
            selectText();
        }
    }

    private static void initTerminal() {
        term.write(Strings.ROBCO, 0, 0);
        term.write(Strings.PASS, 1, 0);
        drawAddrs();
        drawColumns(Strings.WORDS);
        term.setCursorPos(leftBounds.T(), leftBounds.L());
        selectText();
    }

    private static int countMatches(String guess, String answer) {
        int matches = 0;
        for (int i = 0; i < guess.length(); i++) {
            if (guess.charAt(i) == answer.charAt(i)) {
                matches++;
            }
        }
        return matches;
    }

    private static String readOutputLine(int line) {
        int x = outputBounds.B() - line;
        Point start = new Point(x, outputBounds.L());
        Point stop = new Point(x, outputBounds.R());
        return readInColumn(start, stop, outputBounds);
    }

    private static void writeOutputLine(int line, String text) {
        int x = outputBounds.B() - line;
        writeInColumn(text, new Point(x, outputBounds.L()), false, outputBounds);
    }

    private static void appendOutputLine(String text) {
        for (int i = OUT_ROWS - 2; i >= 2; i--) {
            String line = readOutputLine(i);
            writeOutputLine(i + 1, line);
        }
        writeOutputLine(2, Utility.padString(">" + text,
          outputBounds.R() - outputBounds.L() + 1));
    }

    private static void mainLoop() {
        String answer = Strings.WORDS[Utility.randomInt(0, Strings.WORDS.length-1)];
        while (attempts > 0) {
            term.write(Strings.attemptBuilder(attempts), 3, 0, false);
            String guess = getGuess();
            appendOutputLine(selected.text());
            if (guess.equals(answer)) {
                break;
            } else {
                int matches = countMatches(guess, answer);
                appendOutputLine(Strings.DENIED);
                appendOutputLine(String.format(Strings.GRADE, matches, guess.length()));
                attempts--;
            }
        }
    }

    private static void gameOver() {
        if (attempts > 0) {
            for (String s : Strings.CORRECT) {
                appendOutputLine(s);
            }
        } else {
            term.clear();
            term.write(Strings.LOCKED, 10, 19);
            term.write(Strings.ADMIN, 11, 11);
        }
    }

    public static void main(String[] args) {
        initTerminal();
        mainLoop();
        gameOver();
    }
}
