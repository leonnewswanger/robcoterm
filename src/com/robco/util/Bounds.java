package com.robco.util;

import java.awt.Point;

public class Bounds {
    public static enum Direction { LEFT, RIGHT, UP, DOWN }

    private int left;
    private int right;
    private int top;
    private int bottom;

    public Bounds(int left, int right, int top, int bottom) {
        this.left = left;
        this.right = right;
        this.top = top;
        this.bottom = bottom;
    }

    public int L() {
        return this.left;
    }

    public int R() {
        return this.right;
    }

    public int T() {
        return this.top;
    }

    public int B() {
        return this.bottom;
    }

    public Point getBoundedResult(Point p, Direction d) {
        switch (d) {
            case LEFT:
                return Utility.checkBounds(p.y, this.L(), p.x, this.T(), p.x, p.x-1, p.y-1, this.R(), p);
            case RIGHT:
                return Utility.checkBounds(this.R(), p.y, this.B(), p.x, p.x, p.x+1, p.y+1, this.L(), p);
            case UP:
                return Utility.checkBounds(p.x, this.T(), p.x-1, p.y, p);
            case DOWN:
                return Utility.checkBounds(this.B(), p.x, p.x+1, p.y, p);
            default: return p;
        }
    }

    public boolean canMove(Point p, Direction d) {
        switch (d) {
            case LEFT: return p.y > this.L();
            case RIGHT: return p.y < this.R();
            case UP: return p.x > this.T();
            case DOWN: return p.x < this.B();
            default: return false;
        }
    }
}
