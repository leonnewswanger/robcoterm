package com.robco.util;

import java.awt.Point;

public class TextRange {
    private Point x;
    private Point y;
    private String s;

    public TextRange(Point x, Point y, String s) {
        this.x = x;
        this.y = y;
        this.s = s;
    }

    public Point left() {
        return new Point(this.x.x, this.x.y);
    }

    public Point right() {
        return new Point(this.y.x, this.y.y);
    }

    public String text() {
        return this.s;
    }
}
