package com.robco.util;

import java.util.Random;
import java.awt.Point;

public class Utility {
    private static final String RAND_CHARS = "!@#$%^&*()_-[]{};'\",.<>/\\'";
    private static final String ALPHA = "abcdefghijklmnopqrstuvwxyz";

    private static Random rand = new Random();

    public static int randomInt(int min, int max) {
        return rand.nextInt((max - min) + 1) + min;
    }

    public static char randomChar() {
        return RAND_CHARS.charAt(rand.nextInt(RAND_CHARS.length()));
    }

    public static String randomString(int length) {
        StringBuilder builder = new StringBuilder();
        while (builder.length() < length) {
            builder.append(randomChar());
        }
        return builder.toString();
    }

    public static String toHexString(int n) {
        return "0x" + Integer.toHexString(n).toUpperCase();
    }

    public static boolean isAlpha(char c) {
        return ALPHA.indexOf(Character.toString(c).toLowerCase()) != -1;
    }

    public static String padString(String s, int pad) {
        StringBuilder builder = new StringBuilder(s);
        while (builder.length() < pad) {
            builder.append(" ");
        }
        return builder.toString();
    }


    /*
        g greaterThan | l lessThan | tg tiebreakerGreater | tl tiebreakerLess
        x1 return1X | x2 return2X | y1 return1Y | y2 return2Y | o originalPoint
    */
    public static Point checkBounds(int g, int l, int tg, int tl, int x1, int x2,
      int y1, int y2, Point o) {
        if (g > l)
            return new Point(x1, y1);
        else if (g == l && tg > tl)
            return new Point(x2, y2);
        else
            return o;
    }

    public static Point checkBounds(int g, int l, int x, int y, Point o) {
        if (g > l)
            return new Point(x, y);
        else
            return o;
    }
}
